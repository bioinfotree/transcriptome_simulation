#!/usr/bin/env perl

use strict;
use warnings;
use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_db(
    -host => 'ensembldb.ensembl.org',
    -user => 'anonymous'
);

my $stable_id = 'ENST00000393489'; #this is the stable_id of a human transcript
my $transcript_adaptor = $registry->get_adaptor('homo_sapiens', 'core', 'transcript'); #get the adaptor to get the Transcript from the database
my $transcript = $transcript_adaptor->fetch_by_stable_id($stable_id); #get the Transcript object

my $trv_adaptor = $registry->get_adaptor('homo_sapiens', 'variation', 'transcriptvariation'); #get the adaptor to get TranscriptVariation objects
my $trvs = $trv_adaptor->fetch_all_by_Transcripts([$transcript]); #get ALL effects of Variations in the Transcript

foreach my $tv (@{$trvs}) {
	my $tvas = $tv->get_all_alternate_TranscriptVariationAlleles();

	foreach my $tva(@{$tvas}) {
		my @ensembl_consequences;
		my @so_consequences;
		
		my $ocs = $tva->get_all_OverlapConsequences();
		
		foreach my $oc(@{$ocs}) {
			push @ensembl_consequences, $oc->display_term;
			push @so_consequences, $oc->SO_term;
		}
		
		my $sift = $tva->sift_prediction;
		my $polyphen = $tva->polyphen_prediction;
		
		print
			"Variation ", $tv->variation_feature->variation_name,
			 " allele ", $tva->variation_feature_seq,
		         "position", $tv->get_cDNA_position,
			 " has consequence ", join(",", @ensembl_consequences),
			 " (SO ", join(",", @so_consequences), ").";
			 
		if(defined($sift)) {
			print " SIFT=$sift";
		}
		if(defined($polyphen)) {
			print " PolyPhen=$polyphen";
		}
		
		print "\n";
	}
}
