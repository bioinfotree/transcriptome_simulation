#!/usr/bin/env python
#
# Copyright 2012 Paolo Martini <paolo.cavei@gmail.com>

from optparse import OptionParser
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import format_usage, exit, ignore_broken_pipe
import re


    
def iter_rows(fd):
    for line in fd:
        tokens = safe_rstrip(line).split('\t')
        yield tokens
    

def main():
    # gene_id_re       = re.compile(r"gene_id \"([^\"]+)\";")
    # transcript_id_re = re.compile(r"transcript_id \"([^\"]+)\";")
    # exon_number_re   = re.compile(r"exon_number \"([^\"]+)\";")
    # oId_re           = re.compile(r"oId \"([^\"]+)\";")
    # linc_name_re     = re.compile(r"linc_name \"([^\"]+)\";")
    # tss_id_re        = re.compile(r"tss_id \"([^\"]+)\";")
    # class_code_re    = re.compile(r"class_code \"([^\"]+)\";")
    # gene_name_re     = re.compile(r"gene_name \"([^\"]+)\";")

    parser = OptionParser(usage=format_usage('''
             %prog [OPTIONS] <GTF

             Parse in tabular format a GTF format input.


'''))

    options, args = parser.parse_args()
    
    if len(args) != 0:
        exit('Unexpected argument number.')

    required_fields = ["gene_id","transcript_id","exon_number","linc_name","gene_name"]

    for tokens in iter_rows(stdin):
        chrom, source, kind, start, stop, score, strand, value, descr = tokens
        
        optional_fields = descr.rstrip(';').split('; ')
        opt_field = {}
        for couples in optional_fields:
            t = re.split('\s+', couples)
            if len(t) != 2:
                exit('Parsing failed.')

            opt_field[t[0]] = t[1].replace('"', '')

        print '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (chrom, start, stop, strand, opt_field.get('gene_name'), kind, opt_field.get('gene_id'),opt_field.get('transcript_id'),opt_field.get('linc_name'))
        
if __name__=='__main__':
    ignore_broken_pipe(main)

