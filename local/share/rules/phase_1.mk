# Copyright Michele Vidotto 2015 <michele.vidotto@gmail.com>

PRJ1 ?= test
PRJ2 ?= stellatus
ENSEMBL_VERSION ?= 66
USED_SEQ ?= 12155
MIN_LEN ?= 50
COV ?= 4
TAG ?= GNOM



context prj/454_preprocessing

# use cleaned reads from naccarii for newbler (already trimmed)
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned_nb.fasta as SAMPLE1_FASTA
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned_nb.qual as SAMPLE1_QUAL

# use cleaned reads from stellatus for newbler (already trimmed)
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned_nb.fasta as SAMPLE2_FASTA
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned_nb.qual as SAMPLE2_QUAL


## use flux ###############################################################################

genome_features.gtf:
	if ! [ -s $@ ]; then \
	wget -q -O $@.gz http://sammeth.net/confluence/download/attachments/786694/sacCer3_SGDGenes_fromUCSC120515_sorted.gtf.gz \
	&& gunzip $@.gz; \
	fi;


parameters.par:
	wget -q -O $@ http://sammeth.net/confluence/download/attachments/786694/sacCer3_enzyme.par


chromosomes.flag:
	mkdir -p $(basename $@) && cd $(basename $@); \
	if ! [ -s $@ ]; then \
	wget -q -O $(basename $@).tar.gz http://hgdownload.cse.ucsc.edu/goldenPath/sacCer3/bigZips/chromFa.tar.gz \
	&& tar -xvzf $(basename $@).tar.gz; \
	cd ..; \
	touch $@; \
	fi;



flux: parameters.par chromosomes.flag genome_features.gtf
	!threads
	flux-simulator -t simulator --threads $$THREADNUM -x -l -s -p parameters.par


####################################################################################

# link original data in fasta format
.SECONDEXPANSION:
sample%.fasta: $$(SAMPLE%_FASTA)
	ln -sf $< $@
sample%.qual: $$(SAMPLE%_QUAL)
	ln -sf $< $@

# transform original fasta and qual in fastq
454_dat_dir/samples.fastq: sample1.fasta sample1.qual sample2.fasta sample2.qual
	mkdir -p $(dir $@); \
	cat \
	<(qualfa2fastq -q <(sed 's/[ \t]*$$//' $^2) <$< ) \   * use sed to remove blank spaces at the end of quality headers *
	<(qualfa2fastq -q <(sed 's/[ \t]*$$//' $^4) <$^3) \
	>$@

# generate profile for art_454 using the real sequence data
# ./454_readprofile_art input_fastq_files_dir output_profile_dir [fastq_filename_extension]
454_profile_dir: 454_dat_dir/samples.fastq
	$(call LOAD_MODULES); \
	mkdir -p $@; \
	454_readprofile_art $(dir $<) $@ fastq

# get statistics of original sequences
454_dat_dir/samples_fastqc.zip: 454_dat_dir/samples.fastq
	!threads
	$(call LOAD_MODULES); \
	fastqc --format fastq --noextract --threads $$THREADNUM $<





define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "cDNA sequence downloaded from ensembl release $(ENSEMBL_VERSION)" && \
	wget -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	cd .. && \
	touch $3
endef


# download data from ensembl ftp (DANIO cDNA). This data are going to be use for simulation
DANIO_EST = ftp://ftp.ensembl.org/pub/release-$(ENSEMBL_VERSION)/fasta/danio_rerio/cdna/Danio_rerio.Zv9.$(ENSEMBL_VERSION).cdna.all.fa.gz
README = ftp://ftp.ensembl.org/pub/release-$(ENSEMBL_VERSION)/fasta/petromyzon_marinus/cdna/README

danio_EST.flag:
	$(call download_DB, $(DANIO_EST), $(README), $@)

# Recursively download data for all chrs from ensembl ftp (DANIO DNA).
# This data are going to be used for extract transcripts.
DANIO_DNA = ftp://ftp.ensembl.org/pub/release-$(ENSEMBL_VERSION)/fasta/danio_rerio/dna/
README = ftp://ftp.ensembl.org/pub/release-$(ENSEMBL_VERSION)/fasta/danio_rerio/dna/README

danio_DNA.flag:
	mkdir -p $(basename $@) && \
	cd $(basename $@) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' >DB.info && \
	wget -q -A '*.dna.chromosome.*.fa.gz' \
	-R '*.dna_rm.chromosome.*.fa.gz' \
	-R '*.dna.chromosome.MT.fa.gz' \
	$(DANIO_DNA)* && \
	wget -q $(README) && \
	cd .. && \
	touch $@


# sanitizes the fasta files downloaded and produces a list of headers
cDNA.sane.fasta: danio_EST.flag
	$(call LOAD_MODULES); \
	TMP=$$(mktemp tmp.XXXX); \
	TMP1=$$(mktemp tmp.XXXX); \
	zcat $(basename $<)/$(basename $<).gz \
	| sed -e 's/*//' -e '/^$$/d' \   * remove pad, empty lines *
	| fasta2tab \
	| bsort --key=1,1 \
	| bawk -v min_len=$(MIN_LEN) -v used_seq=$(USED_SEQ) '!/^[$$,\#+]/ { \
	if ( ( length($$2) > min_len ) && (NR <= used_seq) )  \   * sequence too short are discarged since art do Segmentation fault (core dumped) those sequences *
	{ print $$0; } }' > $$TMP; \
	cut -f1 <$$TMP \
	| tr [:blank:] \\t >$(basename $@).headers; \    * capture original headers *
	tab2fasta 2 <$$TMP  >$$TMP1; \
	fastatool sanitize $$TMP1 $@; \
	rm $$TMP $$TMP1


cDNA.sane.headers: cDNA.sane.fasta
	touch $@



# from headers of simulation data, extract regions in order to retrieve variants for
# that regions
# chromosome : Zv9 : 25 : 385485 : 417239 : -1
ensembl_regions.tab: cDNA.sane.headers
	bawk '!/^[$$,\#+]/ { \
	split($$3,a,":"); \
	if ( a[3] ~ /^[0-9]/  ) \   * the chromosome position must be a number  *
	printf "%s\t%i:%i-%i\n",$$1,a[3],a[4],a[5]; \   * transcript_id  \t  chromosome:star_region-end_region *
	}' $< \
	| bsort --key=1,1 \
	>$@

from.genomic.cDNA.sane.fasta: danio_DNA.flag ensembl_regions.tab
	$(call LOAD_MODULES); \
	TMP=$$(mktemp tmp.XXXX); \
	TMP1=$$(mktemp tmp.XXXX); \
	>$@; \
	bawk -v tmp=$$TMP '!/^[$$,\#+]/ { \
	split($$2,a,":"); \
	split(a[2],b,"-"); \
	printf "slicing transcripts: chr:%i\tstart:%i\tend:%i\n",a[1],b[1],b[2]; \
	cmd_1="zcat <$(basename $<)/Danio_rerio.Zv9.66.dna.chromosome."a[1]".fa.gz \
	| seqret -sformat fasta -filter -sbegin "b[1]" -send "b[2]" \
	| descseq -sformat fasta -filter -name \""$$1"\" >>"tmp; \
	system(cmd_1); \
	}' $^2 \
	&& cat $$TMP \
	| sed -e 's/*//' -e '/^$$/d' \   * remove pad, empty lines *
	| fasta2tab \
	| bsort --key=1,1 \
	| bawk -v min_len=$(MIN_LEN) -v used_seq=$(USED_SEQ) '!/^[$$,\#+]/ { \
	if ( ( length($$2) > min_len ) && (NR <= used_seq) )  \   * sequence too short are discarged since art do Segmentation fault (core dumped) those sequences *
	{ print $$0; } }' \
	| tab2fasta 2 >$$TMP1 \
	&& fastatool sanitize $$TMP1 $@ \
	&& rm $$TMP1 $$TMP


# use list of regions for obtain corresponding variants from ensembl

# chromosome:star_region-end_region
# 25:18278-28255
# 25:33034-38736
# ...

# excluded regions
# Zv9_NA963:1:14235
# lsZv9_NA358:1:2927
# ...

# excluded regions
# MT:3803:4777# MT:4993:6039
# ...

.PRECIOUS: variations.gff
variations.gff: ensembl_regions.tab
	with_ensembl_api $(ENSEMBL_VERSION) region_report --db_version=$(ENSEMBL_VERSION) --species=Danio --gff --verbose --input=<(cut -f2 $<) --output=$@ --include=v

# variations.gff.test:
# 	with_ensembl_api $(ENSEMBL_VERSION) region_report --db_version=$(ENSEMBL_VERSION) --species=Danio --gff --verbose --chromosome=9 --start=29975916 --end=30036561  --include=vp


# .PRECIOUS: structural_var.gff
# structural_var.gff: ensembl_regions.tab
# 	with_ensembl_api $(ENSEMBL_VERSION) region_report --db_version=$(ENSEMBL_VERSION) --species=Danio --gff --verbose --input=<(cut -f2 $<) --output=$@ --include=s


# use variant list, region list and simulation. Select variations that fall within the coding regions. Replaces random positions in transcripts, with the variants. Replaces a number of positions equal to the number of variations in coding regions. The variant replaced is taken at random from the possible 4 nucleotides so they are only SNPs. The position is replaced with an allele that must be different from the one in reference at the position indicated. A new fasta file is produced, ready for simulation. It contains sequences with inserted variants and unchanged sequences. Sequences for which there are no variants in codig regions remains unchanged. A prefix is added to every sequence ID, both in the output fasta file and the report
$(TAG)%.fasta: ensembl_regions.tab variations.gff from.genomic.cDNA.sane.fasta
	haplo_builder --reg $< --var $^2 --tag $(basename $@) --rep $(basename $@).report <$^3 \
	| sed -e 's/-//g' >$@   * remove single base deletions inserted by haplo_builder as - *


$(TAG)%.report: $(TAG)%.fasta
	touch $@




# simulate SINGLE-END READS usign generated haplotipes
# art_454  [-a] [-s] [-t] [ -p read_profile ] [ -c num_flow_cycles ] <INPUT_SEQ_FILE> <OUTPUT_FILE_PREFIX> <FOLD_COVERAGE>
$(TAG)%/$(TAG).fq: $(TAG)%.fasta 454_profile_dir
	$(call LOAD_MODULES); \
	mkdir -p $(dir $@); \
	cd $(dir $@); \
	art_454 -a -s -p ../$^2 -t ../$< $(basename $(notdir $@)) $(COV); \
	cd ..

# I don't know why, but art_454 insert fastq headers with empty sequences at random position in the fastq file. This could depend by the use of costum profile. Here I cleaned this sequences
$(TAG)%/$(TAG).fq.cleaned: $(TAG)%/$(TAG).fq
	fastq2tab -e <$< \
	| bawk ' !/^[$$,\#+]/ { \
	if (length($$2)) { print $$0; } \
	}' \
	| tab2fastq >$@



# create strain file for MIRA
$(TAG)%/$(TAG).strain: $(TAG)%/$(TAG).fq.cleaned
	fastq2tab <$< \
	| bawk ' !/^[$$,\#+]/ { \
	split($$1,a,"_"); \
	printf "%s\t%s\n", $$1, a[1]; \
	}' > $@




# get statistics of simulated sequences
$(TAG)%/$(TAG).fq.cleaned_fastqc.zip: $(TAG)%/$(TAG).fq.cleaned
	!threads
	$(call LOAD_MODULES); \
	fastqc --format fastq --noextract --threads $$THREADNUM $<








.PHONY: test
test:
	@echo $(basename  $(TAG)%/$(TAG).strain);
	@echo $(dir $(TAG)%/$(TAG)%.fq.clened);


# This should be the default target.
ALL   += 454_dat_dir/samples.fastq \
	 454_profile_dir \
	 454_dat_dir/samples_fastqc.zip \
	 danio_EST.flag \
	 cDNA.sane.fasta \
	 variations.gff \
	 \
	 $(TAG)1.fasta \
	 $(TAG)1.report \
	 $(TAG)1/$(TAG).fq.cleaned \
	 $(TAG)1/$(TAG).strain \
	 $(TAG)1/$(TAG).fq.cleaned_fastqc.zip \
	 \
	 $(TAG)2.fasta \
	 $(TAG)2.report \
	 $(TAG)2/$(TAG).fq.cleaned \
	 $(TAG)2/$(TAG).strain \
	 $(TAG)2/$(TAG).fq.cleaned_fastqc.zip \
	 \
	 $(TAG)3.fasta \
	 $(TAG)3.report \
	 $(TAG)3/$(TAG).fq.cleaned \
	 $(TAG)3/$(TAG).strain \
	 $(TAG)3/$(TAG).fq.cleaned_fastqc.zip \
	 \
	 $(TAG)4.fasta \
	 $(TAG)4.report \
	 $(TAG)4/$(TAG).fq.cleaned \
	 $(TAG)4/$(TAG).strain \
	 $(TAG)4/$(TAG).fq.cleaned_fastqc.zip




# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=






# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += 454_dat_dir/samples.fastq \
	 454_profile_dir \
	 454_dat_dir/samples_fastqc.zip \
	 danio_EST.flag \
	 cDNA.sane.fasta \
	 cDNA.sane.headers

# clean directories
CLEAN_FULL += $(TAG)1 \
	      $(TAG)2 \
	      $(TAG)3 \
	      $(TAG)4

